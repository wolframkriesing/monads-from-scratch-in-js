// @flow
const assert = require('assert');
const { describe, it } = require('mocha');

type MaybeType<T> = {|
  bind: <F>(T => MaybeType<F>) => MaybeType<F>;
  map: <F>(T => F) => MaybeType<F>;
  map2: (MaybeType<T>, (any, any) => any) => MaybeType<T>;
|};

const Maybe = {
  of: (element) => {
    if (element) {
      return Maybe.some(element);
    } else {
      return Maybe.nothing();
    }
  },
  some: <T>(element: T): MaybeType<T> => ({
    bind: <F>(mapFn: T => MaybeType<F>): MaybeType<F> => mapFn(element),
    map: <F>(fn: T => F): MaybeType<F> => Maybe.some(fn(element)),
    map2: (maybeOtherElement, combinatorFn) => {
      return maybeOtherElement
        .map((otherElement) => combinatorFn(element, otherElement));
    }
  }),
  nothing: <T>(): MaybeType<T> => ({
    bind: <F>(a :T => MaybeType<F>): MaybeType<F> => Maybe.nothing(),
    map: <F>(fn: T => F): MaybeType<F> => Maybe.nothing(),
    map2: () => Maybe.nothing(),
  }),
};
const head = <A>(list: Array<A>): MaybeType<A> => {
  if (list.length === 0) {
    return Maybe.nothing();
  }
  return Maybe.some(list[0]);
};

const assertIsNothing = (maybeHead) => {
  maybeHead.map(() => {
    throw Error('Should not be called');
  });
};

const assertIsSome = (maybeElement, element) => {
  let fnCalled = false;
  maybeElement.map((value) => {
    assert.deepEqual(value, element);
    fnCalled = true;
  });
  assert(fnCalled);
};
describe('head()', function() {
  this.timeout(20);
  it('of an empty list is nothing', () => {
    const emptyList = [];
    assertIsNothing(head(emptyList));
  });
  it('the head of a list with one element, returns a Some of that element', () => {
    const element = 42;
    const listWithOneElement = [element];
    const maybeElement = head(listWithOneElement);
    assertIsSome(maybeElement, element);
  });

  describe('a real-life use case', () => {
    it('map returns nothing mapping nothing', () => {
      const fn = () => { throw Error() };
      assertIsNothing(Maybe.nothing().map(fn));
    });
    it('upper case first element of a list', () => {
      const list: Array<string> = ['a', 'b', 'c'];
      const upperCaseFn = s => s.toUpperCase();
      const maybeUpped = head(list).map(upperCaseFn)
        .map((s) => s + '!');
      assertIsSome(maybeUpped, 'A!');
    });
  });

  describe('concat the heads of two lists', () => {
    const firstNames = [
      'Chris', 'Yannick'
    ];
    const lastNames = [
      'FP'
    ];
    const concatNames = (firstName, lastName) =>
      firstName + ' ' + lastName;
    it('full name of first person', () => {
      const maybeFullname = head(firstNames).map2(head(lastNames), concatNames);
      assertIsSome(maybeFullname, 'Chris FP');
    });
    it('returns no name when last name is missing', () => {
      const maybeFullname = head(['Yannick']).map2(head([]), concatNames);
      assertIsNothing(maybeFullname);
    });
  });

  describe('loading orders for a client', () => {
    type Product = { id: number };
    const loadOrders = (clientId) => {
      if (clientId === 42) {
        const element: Array<Product> = [{id:1},{id:2}];
        return Maybe.some(element);
      }
      return Maybe.nothing();
    };
    const extractClientId = (queryObject) => Maybe.of(queryObject.clientId);
    it('returns nothing if we are not given a clientId', () => {
      const queryObject = {};
      const maybeClientId = extractClientId(queryObject);
      const maybeOrders = maybeClientId.bind(loadOrders);
      assertIsNothing(maybeOrders)
    });

    it('returns orders for client id', () => {
      const orders = [{id:1},{id:2}];
      const queryObject = {clientId: 42};
      const maybeClientId = extractClientId(queryObject);
      const maybeOrders = maybeClientId.bind(loadOrders);
      assertIsSome(maybeOrders, orders);
    });
    it('returns nothing for an unknown clientId', () => {
      const queryObject = {clientId: 23};
      const maybeClientId = extractClientId(queryObject);
      const maybeOrders = maybeClientId.bind(loadOrders);
      assertIsNothing(maybeOrders);
    });
  })
});
