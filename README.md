# Monads from scratch

Was started at [#busconf17] with the intention to come up with monads from a real use case.
So basically building one ourselves from scratch. We tried to always have something
that is at least kind of real world example. We might not always succeeded, but
we reminded ourselves often, so the tests are not too esotheric, I hope :).

And we did. Unfortunately we did NOT commit every step on our way, so the progress
is a bit hard to follow.
But the good thing is, if you want to follow the tests from top to down you might
be able to walk the path as we did. It took us about 4h to come up with the first
set of code, that will be in the initial commit.

## Setup

### Using nix

If you don't have [nix as package manager][nix] yet, [install it now][install-nix]! Imho it's the light weight
version of docker :).

- `cd monads-from-scratch-in-js` change into this project's directory
- `nix-shell` start the nodejs8 env
- `npm i` install all the needed packages
- `npm test` runs all type checks, builds and runs the tests

### Without using nix

Make sure to have a later nodejs (works with v8) version installed.

- `cd monads-from-scratch-in-js` change into this project's directory
- `npm i` install all the needed packages
- `npm test` runs all type checks, builds and runs the tests

## How to work with it?

I propose to remove all the implementation and just start with the tests and 
turn one after another green. We left the types always to a later stage.
We had basic function types and the `MaybeType` always correct, but filled in `any`
often, when we were just not too sure how to do it right. And we focused on it
at some later point.

It really helps if you have some FP expert sitting aside and get input on various
things and also a little bit on the whys of what is going on. Also the function names
we used came from FP experienced people, so they might not look "right" in the beginning.

[#busconf17]: https://twitter.com/search?q=%23busconf17
[install-nix]: https://nixos.org/nix/manual/#chap-quick-start
[nix]: https://nixos.org/nix/